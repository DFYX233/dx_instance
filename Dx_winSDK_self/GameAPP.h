#ifndef GAMEAPP_H
#define GAMEAPP_H
#include "d3dApp.h"
#include "lgihtClass.h"
#include "Camera.h"
#include "MeshGeometryClass.h"
#define MAX_INSTANCE_CONSTANTS 100
#define IMAGE_HEIGHT 512
#define IMAGE_WIDTH 512
class GameApp : public D3DApp
{
public:
    GameApp(HINSTANCE hInstance);
    ~GameApp();

    bool Init();
    void OnResize();
    void UpdateScene(float dt);
    void DrawScene();


    //struct VSConstantBuffer
    //{
    //    DirectX::XMMATRIX world;
    //    DirectX::XMMATRIX view;
    //    DirectX::XMMATRIX proj;
    //    DirectX::XMMATRIX worldInvTranspose;
    //};
    //struct PSConstantBuffer
    //{
    //    DirLit dirlit;
    //};

    struct CBufferReFreashFrequently
    {
        DirectX::XMMATRIX world;
        DirectX::XMMATRIX view;
        DirectX::XMMATRIX worldInvTranspose;
        DirectX::XMMATRIX boneTransform[100];
    };
    struct CBufferReFreashRarely
    {
        DirLit dirlit;
        DirectX::XMINT4 g_InstanceMatricesWidth;
    };
    struct CBufferReFreshOnResize
    {
         DirectX::XMMATRIX proj;
    };
    struct InstanceDataElelment
    {
        DirectX::XMFLOAT4 world1;
        DirectX::XMFLOAT4 world2;
        DirectX::XMFLOAT4 world3;
        DirectX::XMFLOAT4 color;

        DirectX::XMUINT2 indexAndOffset;
        DirectX::XMUINT2 animationIndexAndEndOffset;
    };
    struct CBufferInstance
    {
        InstanceDataElelment instancePos[MAX_INSTANCE_CONSTANTS];
    };

private:
    bool InitEffect();
    bool InitResource();

private:
    ComPtr<ID3D11InputLayout> m_pVertexLayout;	// 顶点输入布局
    ComPtr<ID3D11Buffer> m_pVertexBuffer;		// 顶点缓冲区
    ComPtr<ID3D11VertexShader> m_pVertexShader;	// 顶点着色器
    ComPtr<ID3D11PixelShader> m_pPixelShader;	// 像素着色器
    ComPtr<ID3D11Buffer> m_pindexBuffer;

    ComPtr<ID3D11VertexShader> m_pVertexShader2;	// 顶点着色器
    ComPtr<ID3D11PixelShader> m_pPixelShader2;	// 像素着色器

    ComPtr <ID3D11Buffer> m_pConstantBuffer[4];


    ComPtr<ID3D11ShaderResourceView> m_pWood;
    ComPtr<ID3D11ShaderResourceView> m_pAnimaTexture;
    ComPtr<ID3D11SamplerState> m_pSamplerState;
    ComPtr<ID3D11RasterizerState> m_pRsaterizarState;

    std::shared_ptr<Camera> m_pCamera;
    CameraMode m_cameraMode;
    
    CBufferReFreashFrequently m_cBufferFrequently;
    CBufferReFreashRarely m_cBufferRarely;
    CBufferReFreshOnResize m_cBufferResize;
    CBufferInstance m_cBufferInstance;
    std::vector<InstanceDataElelment> m_instanceData;

    DirLit m_dirLit;
    
    MeshGeometryClass* g1;
    MeshGeometryClass *plane;
    GameTimer timer;

    bool isFinshed;
    bool needBake;
    bool isStart;
    int animaFramesCount;
    int currentFrames;
    int FPS;
    int starOffset;
    int perRowBoneAmount;
    float* m_pixel;
    float timeCount;
    uint8_t* uintData;
    float maxValue;
    void BakeBoneImage();
};

inline void FloatToRGBE(_Out_writes_(width * 4) uint8_t* pDestination, _In_reads_(width* fpp) const float* pSource, size_t width, _In_range_(3, 4) int fpp) noexcept
{
    auto ePtr = pSource + width * size_t(fpp);

    for (size_t j = 0; j < width; ++j)
    {
        if (pSource + 2 >= ePtr) break;
        const float r = pSource[0] >= 0.f ? pSource[0] : 0.f;
        const float g = pSource[1] >= 0.f ? pSource[1] : 0.f;
        const float b = pSource[2] >= 0.f ? pSource[2] : 0.f;
        pSource += fpp;

        const float max_xy = (r > g) ? r : g;
        float max_xyz = (max_xy > b) ? max_xy : b;

        if (max_xyz > 1e-32f)
        {
            int e;
            max_xyz = frexpf(max_xyz, &e) * 256.f / max_xyz;
            e += 128;

            const uint8_t red = uint8_t(r * max_xyz);
            const uint8_t green = uint8_t(g * max_xyz);
            const uint8_t blue = uint8_t(b * max_xyz);

            pDestination[0] = red;
            pDestination[1] = green;
            pDestination[2] = blue;
            pDestination[3] = (red || green || blue) ? uint8_t(e & 0xff) : 0u;
        }
        else
        {
            pDestination[0] = pDestination[1] = pDestination[2] = pDestination[3] = 0;
        }

        pDestination += 4;
    }
}



#endif
