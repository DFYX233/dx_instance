#include "light.hlsli"
#define MAX_INSTANCE_CONSTANTS 100

Texture2D gTex : register(t0);
SamplerState gSampler : register(s0);

//cbuffer ConstantBuffer : register(b0)
//{
//	matrix world;
//	matrix view;
//	matrix proj;
//	
//}
//
//cbuffer PSConstanceBuffer : register(b1)
//{
//	DirectLight dirlit;
//}
struct PerInstanceData
{
	float4 world1;
	float4 world2;
	float4 world3;
	float4 color;
	uint4 animationData;
};

cbuffer CBufferReFreashFrequently:register(b0)
{
	matrix world;
	matrix view;
	matrix worldInvTranspose;
	matrix boneTransform[100];
}

cbuffer CBufferReFreashRarely: register(b1)
{
	DirectLight dirlit;
}

cbuffer CBufferReFreshOnResize: register(b2)
{
	matrix proj;
}
cbuffer CBufferInstance : register(b3)
{
	PerInstanceData g_instance[MAX_INSTANCE_CONSTANTS];
}

struct VertexPosTex
{
	float3 pos : POSITION;
	float2 uv : TEXCOORD;
};
struct VertexPosHWNormalTex
{
	float4 pos : SV_POSITION;
	float3 posW : POSITION;
	float3 normalW : NORMAL;
	float2 uv : TEXCOORD;
};

struct	VertexIn
{
	float3 pos : POSITION;
	float4 normal : NORMAL;
	float2 uv : TEXCOORD;
};

struct SkinnedVertexIn
{
	float3 pos : POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD;
	float4 weights : WEIGHTS;
	uint4 boneIndiecs : BONEINDICES;
	uint instanceId : SV_InstanceID;
};

struct V2f
{
	float4 pos : SV_POSITION;
	float4 color :COLOR;
};

float4x4 g_Identity = float4x4(1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f);

