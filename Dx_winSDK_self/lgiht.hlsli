

struct DirLit
{
	float3 direction;
	float intensity;
};

struct PointLit
{
	float3 position;
	float range;

	float3 attenuation;
	float intensity;
};


float4 CalcDirLight(Mat mat, DirLit l, float normal, float toEye)
{
	float3 lightDir = -l;
}